# RPG Dice Roller

![screenshot](screenshot.png)


Simple dice rolling app for table top RPGs such as Dungeons and Dragons or [FundamentalRPG](https://gitlab.com/noahmigoski/fundamentalrpg).
To run it you need to [install racket](https://download.racket-lang.org/)

You can use `sudo apt install racket` on Debian and Ubuntu or `brew install racket` on macos with homebrew.
Then download this repository and run:

`racket dice_roller.rkt`

If you want to create an executable you can run:

`raco exe --gui dice_roller.rkt`

The linux executable is available on my website [here](https://www.migoski.net/dice_roller.html). Note this executable is sometimes out of date and will only work on the x86 architecture.
